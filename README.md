# Zabbix-Winupdates


> Fork: https://github.com/SpookOz/zabbix-winupdates
>
> Autor: Davis Mendoza Paco **<davis.men.pa@gmail.com> / <davis.mp@yandex.com>**

Este es una plantilla de Zabbix para monitorear actualizaciones de Sistemas operativos Windows

Ha sido probado en Zabbix v5.2.7 con Windows server 2008 R2, 2016 y 2019

## Características

- Funciona con agentes activos
- Comprueba si hay actualizaciones e informa los números de cada una.
- Informa la fecha de las últimas actualizaciones.
- Incluye un panel para el Tablero.
- Incluye disparadores (Triggers) para advertir sobre los estados de actualización.

## Requisitos

- Solo se ha probado con agentes activos Zabbix.
- Tener instalado zabbix-sender

## Archivos incluidos

- **WinupdatesV5.xml:** Plantilla para importar a zabbix
- **check_win_updates-v5.ps1:** bash, Debe colocarse en el directorio de "plugins" en la carpeta de instalación de Zabbix Agent 

## Instalación

Crear el directorio de plugins
```bash
mkdir 'C:\Program Files\Zabbix Agent\plugins\'
```

Descargar el script y copiar a la carpeta creada
```sh
move check_win_updates-v5.ps1 'C:\Program Files\Zabbix Agent\plugins\'
```

editar el archivo 
```bash
check_win_updates-v5.ps1
```

Cambiar el valor de la variable, por el nombre del servidor registrado en zabbix
```bash
$SrvHost = "srv-xxx"
```

## Configuración

Habilitar la ejecución de comandos remotos 

> https://www.zabbix.com/documentation/5.2/en/manual/config/items/restrict_checks
 
Editar el archivo de configuración
```bash
zabbix_agent.conf
```

Adicionar la siguiente linea en la configuración
```bash
AllowKey=system.run[*]
```

Para que los cambios surtan efecto, reiniciar el servicio
```bash
zabbix-agent
```

## Probar 

Para verificar si el script funciona correctamente, abrir la terminal de PowerShell y realizar la prueba ejecutando
```bash
cd 'C:\Program Files\Zabbix Agent\plugins\'

.\check_win_updates-v5.ps1
```

El resultado del comando nos muestra lo siguiente:
```bash
zabbix_sender [14058]: DEBUG: answer [{"response":"success","info":"processed: 1; failed: 0; total: 1; seconds spent: 0.000057"}]
info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000057"
sent: 1; skipped: 0; total: 1
```

Del resultado obtenido, se puede identificar el resultado correcto con **0**  fallas
```sh
"response":"success","info":"processed: 1; failed: 0
```

![](img/powershell.png)

## Dashboard

Puede adicionar un widget a su Dashboard eligiendo "Data overview" and "application" "Winupdates-Panel"

![](img/zabbix-panel.png)
